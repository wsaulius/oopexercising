package Shop.FirstPrototype;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class OurFirstPrototypeAllinone {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        //konstruoja is Product.java kuris dabar bendras ir su nauja versija
        Product[] menu_ = new Product[]{
                new Product("milk", 1.15),
                new Product("bread", 0.80),
                new Product("cheese", 1.30),
                new Product("potato", 0.70),
                new Product("tea", 2.10)
        };
        List cart = new ArrayList();
        double cartPrice = 0;
        do {

            //Simulate the process of doing shopping:

            String temp = in.next();
            for (int i = 0; i < menu_.length; i++) {
                //ask for a product
                if (menu_[i].getName().equals(temp)) {
                    //add it to the cart (array)
                    cart.add(menu_[i].getName() + " " + menu_[i].getPrice());
                    cartPrice += menu_[i].getPrice();
                    System.out.println("Ar nori dar ko nors?");
                }
            }
        } while (in.next().equals("y"));

        System.out.println("Musu krepselio turinys: ");
        for (int i = 0; i < cart.size(); i++) {
            System.out.println("\t" + cart.get(i));
        }

        System.out.printf("Bendra suma: %.2f", cartPrice);
    }

}
