package Shop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ShoppingOperations {
    Scanner in = new Scanner(System.in);

    //Iskvieciame klase, kurioje saugomos visos prekes(objektai), kurie sudeti i masyva
    ProductMenu shopItems = new ProductMenu();

    //prekiu krepselis, i kuri desime prekes
    List<String> cart = new ArrayList<String>();

    //krepselio suma
    double cartPrice = 0;

//Metodai---------------------------------------------------------------

    public void run() {
        welcomeMessage();
        question();
        finalCart();
        farewellMessage();
    }

    //pasveikinimo metodas
    public void welcomeMessage() {
        System.out.println("Sveiki atvyke i Maxima!");
    }

    //atsisveikinimo metodas
    public void farewellMessage() {
        System.out.println("\nDekui, kad apsilankete!");
    }

    //metodas uzduoda klausima, ivedame numeri priklausomai nuo norimo pasirinkimo
    //kvieciame metodus, priklausomai nuo ivesto numerio
    //jei neteisingas skaicius, kartoja metoda nuo pradziu
    public void question() {
        System.out.println("\nKa noretumete atlikti?");
        System.out.println("\t1 - rodyti parduotuveje esancias prekes");
        System.out.println("\t2 - ideti preke");
        System.out.println("\t3 - rodyti krepseli");
        System.out.println("\t4 - iseiti is parduotuves");
        System.out.println("\t5 - pasalinti preke is krepselio");
        System.out.print("\nIveskite skaiciu: ");
        String temp1 = in.next();
        switch (temp1) {
            case "1":
                shopItems.pilnaPrekiuInfo();
                question();
                break;
            case "2":
                addingtocart();
                question();
                break;
            case "3":
                showCart();
                question();
                break;
                case "4":
                break;
            case "5":
                removeFromCart();
                showCart();
                question();
                break;
            default:
                System.out.println("\nNeteisingas pasirinkimas");
                question();
                break;
        }
    }

    //metodas ideti preke i krepseli
    public void addingtocart() {
        System.out.println("Iveskite prekes pavadinima, kuria norite ideti: ");
        String temp = in.next();
        for (int i = 0; i < shopItems.listOfProducts.length; i++) {
            //ask for a product
            if (shopItems.listOfProducts[i].name.equals(temp)) {
                //add it to the cart (array)
                cart.add(shopItems.listOfProducts[i].name + " - " + shopItems.listOfProducts[i].price);
                cartPrice += shopItems.listOfProducts[i].price;
            }
        }
    }

    //metodas parodantis musu krepseli ir prekiu suma
    public void showCart() {
        if(cart.size()>0) {
            System.out.println("\n#############################");
            System.out.println(" Krepselyje yra " + cart.size() + " prekes:");
            for (int i = 0; i < cart.size(); i++)
                 {
                System.out.println("\t" + (i+1) + ". " + cart.get(i).toUpperCase());

            }
            System.out.println(" ------------------------");
            System.out.printf(" Pirkiniu suma: %.2f", cartPrice);
            System.out.println("\n#############################");
        } else {
            System.out.println("Jusu krepselis tuscias.");
        }
    }

    //pasirinkus iseiti is parduotuves, atvaizduoja galutni pirkiniu krepseli
    public void finalCart() {
        System.out.println("\nJus sekmingai baigete apsipirkima!");
        showCart();
    }

    public void removeFromCart() {
        System.out.println("Kuria preke norite pasalinti?");
        System.out.print("Iveskite numeri: ");
        int temp = in.nextInt() -1;
        String temp1 = cart.get(temp);
        cart.remove(temp);
        System.out.println("Jus sekmingai pasalinote " + temp1);
    }

}
