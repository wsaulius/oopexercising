package Shop;


import java.util.Arrays;

public class ProductMenu {

    Product[] listOfProducts = new Product[]{
            new Product("milk", 1.15),
            new Product("bread", 0.80),
            new Product("cheese", 1.30),
            new Product("potato", 0.70),
            new Product("tea", 2.10)
    };

    //bendras metodas parodyti prekiu kieki ir prekiu lista
    public void pilnaPrekiuInfo() {
        numberOfProductsInShop();
        namesAndPricesOfAllProductsInShop();
    }

    //Atvaizduojame prekiu kieki paimdami prekiu Array length
    public void numberOfProductsInShop() {
        System.out.println("\nParduotuvėje yra " + listOfProducts.length + " prekės.");
    }

    //Isvedame visas turimas prekes
    public void namesAndPricesOfAllProductsInShop() {
        for (int i = 0; i < listOfProducts.length; i++) {
            System.out.println("\t> " + listOfProducts[i].name.toUpperCase() + " - 1kg kaina " + listOfProducts[i].price + "Eur");
        }
    }
}


